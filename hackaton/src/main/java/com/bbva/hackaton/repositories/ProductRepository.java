package com.bbva.hackaton.repositories;

import com.bbva.hackaton.HackatonApplication;
import com.bbva.hackaton.models.ProductModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ProductRepository {

    public List<ProductModel> findAll() {
        System.out.println("findAll en ProductRepository");
        return HackatonApplication.productModels;
    }

    public Optional<ProductModel> findById(String id){
        System.out.println("findById en ProductRepository ");

        Optional<ProductModel> result = Optional.empty();

        for(ProductModel productInList: HackatonApplication.productModels){
            if (productInList.getId().equals(id)) {
                System.out.println("Producto encontrado");
                result = Optional.of(productInList);
            }
        }

        return result;
    }


}
