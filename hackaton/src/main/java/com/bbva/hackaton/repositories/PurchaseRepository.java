package com.bbva.hackaton.repositories;

import com.bbva.hackaton.HackatonApplication;
import com.bbva.hackaton.models.PurchaseModel;
import com.bbva.hackaton.services.ProductService;
import com.bbva.hackaton.services.ServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class PurchaseRepository {

    public List<PurchaseModel> findAll(){
        System.out.println("findAll de PurchaseRespository");
        return HackatonApplication.purchaseModels;
    }

    public PurchaseModel save(PurchaseModel purchase){
        System.out.println("Save en PurchaseRepository");

        HackatonApplication.purchaseModels.add(purchase);

        return purchase;
    }

    public Optional<PurchaseModel> findById(String id) {
        System.out.println("findById en PurchaseRepository");

        Optional<PurchaseModel> result = Optional.empty();

        for(PurchaseModel purchaseInList: HackatonApplication.purchaseModels){
            if (purchaseInList.getId().equals(id)) {
                System.out.println("Compra encontrada");
                result = Optional.of(purchaseInList);
            }
        }

        return result;
    }

}
