package com.bbva.hackaton.repositories;

import com.bbva.hackaton.HackatonApplication;
import com.bbva.hackaton.models.UserModel;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepository {
    public List<UserModel> findAllUsers(){
        System.out.println("findAllUsers en ProductRepository");

        return HackatonApplication.userModels;
    }
}
