package com.bbva.hackaton.controllers;

import com.bbva.hackaton.models.ProductModel;
import com.bbva.hackaton.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
@CrossOrigin(origins="*",methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT,RequestMethod.PATCH})
public class ProductController {
    @Autowired
    ProductService productService;

    @GetMapping("/productos")
    public ResponseEntity<List<ProductModel>> getProducts() {
        System.out.println("getProducts de controllers");

        return new ResponseEntity<>(
                this.productService.findAll(),
                HttpStatus.OK
        );
    }

}
