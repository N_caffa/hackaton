package com.bbva.hackaton.controllers;

import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins="*",methods = {RequestMethod.GET})
public class HelloController {

    @RequestMapping("/")// crear ruta
    public String index(){
        return "Hola mundo desde Hackaton";
    }

    @RequestMapping("/hello")//@RequestParam(value = "name",defaultValue = "Tech U") se pone para requerir el parametro de entrada
    public String hello(@RequestParam(value = "name",defaultValue = "Hackaton") String name){
        return String.format("hola %s",name);
    }

}
