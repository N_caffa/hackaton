package com.bbva.hackaton.controllers;

import com.bbva.hackaton.models.PurchaseModel;
import com.bbva.hackaton.services.PurchaseService;
import com.bbva.hackaton.services.ServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(origins="*",methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT,RequestMethod.PATCH})
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @GetMapping("/carritos")
    public ResponseEntity<Object> getPurchases() {
        System.out.println("getPurchases de PurchaseControlers");

        return new ResponseEntity<>(purchaseService.findAll(),HttpStatus.OK);
    }

    
    @PostMapping("/carritos")
    public ResponseEntity<Object> addPurchase(@RequestBody PurchaseModel purchase){

        System.out.println("addShop");
        System.out.println("");

        ServiceResponse serviceResponse = this.purchaseService.addShop(purchase);

        return new ResponseEntity<>(
                serviceResponse.getResponseStatus()==HttpStatus.OK ? serviceResponse.getPurchase(): serviceResponse.getMsg(),
                serviceResponse.getResponseStatus()
         );
    }



}
