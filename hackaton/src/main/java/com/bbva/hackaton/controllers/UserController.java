package com.bbva.hackaton.controllers;

import com.bbva.hackaton.models.UserModel;
import com.bbva.hackaton.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

// Añadimos el RestController y Requestmapping para que sea accesible entre controladores y el front
@RestController
// esta es la url para realizar las pruebas y la conexion
@RequestMapping ("/user")
public class UserController {

    // añadimos el autowired para que se haga la llamada de forma automatica al servicio
    @Autowired
    UserService userService;

        // Consulta de usuarios
        // Consulta de usuarios por edad
        @GetMapping("/usuarios")
        public ResponseEntity<List<UserModel>> getUsers(){
            System.out.println("ResponseEntity getUsers()");

            return new ResponseEntity<>(
                    this.userService.findAllUsers(),
                    HttpStatus.OK
            );
        }
}
