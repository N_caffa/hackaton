package com.bbva.hackaton.models;

import java.util.ArrayList;
import java.util.Map;

public class PurchaseModel {
    private String id;     //Identificador de la compra
    private String userid; //Usuario que realizaa la compra
    private float amount;  //Precio total de la compra
    private Map<String,Integer> purchaseItems; //Lista de items comprados

    public PurchaseModel() {
    }

    public PurchaseModel(String id, String userid, float amount, Map<String,Integer> purchaseItems) {
        this.id = id;
        this.userid = userid;
        this.amount = amount;
        this.purchaseItems = purchaseItems;
    }

    public void setId(String id){
        this.id = id;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public void setPurchaseItems (Map<String,Integer> purchaseItems) {
        this.purchaseItems = purchaseItems;
    }

    public String getId() {
        return id;
    }

    public String getUserid() {
        return userid;
    }

    public float getAmount() {
        return amount;
    }

    public Map<String,Integer> getPurchaseItems() {
        return purchaseItems;
    }
}
