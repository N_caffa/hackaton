package com.bbva.hackaton.models;

public class ProductModel {

    private String id;
    private String name;
    private String desc;
    private double price;
    private String foto;
    private int stock;

    public ProductModel() {
    }

    public ProductModel(String id, String name, String desc, double price, String foto, int stock) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.price = price;
        this.foto = foto;
        this.stock = stock;
    }

    public Boolean restaStock(int items){
        System.out.println(" Dentro del metodo restar "+ items);
        Boolean flags = true;
        if(items > this.stock ){
            System.out.println(" Dentro del metodo restar "+ flags);
            flags = false;
            return flags;
        }else{
            this.stock -= items;
        }
        return flags;

    }

    public void sumaStock(int items){
        this.stock += items;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public double getPrice() {
        return price;
    }

    public String getFoto() {
        return foto;
    }

    public int getStock() {
        return stock;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDesc(String desc){
        this.desc = desc;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}