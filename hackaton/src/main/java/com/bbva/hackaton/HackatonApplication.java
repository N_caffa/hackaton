package com.bbva.hackaton;

import com.bbva.hackaton.models.ProductModel;
import com.bbva.hackaton.models.PurchaseModel;
import com.bbva.hackaton.models.UserModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class HackatonApplication {

	public static ArrayList<ProductModel> productModels;
	public static ArrayList<UserModel> userModels;
	public static ArrayList<PurchaseModel> purchaseModels;

	public static void main(String[] args) {

		SpringApplication.run(HackatonApplication.class, args);
		HackatonApplication.productModels = HackatonApplication.getTestData();
		HackatonApplication.userModels = HackatonApplication.getUserData();
		HackatonApplication.purchaseModels = HackatonApplication.getTestPurchases();
	}

	private static ArrayList<ProductModel> getTestData(){
		ArrayList<ProductModel> productModels = new ArrayList<>();
		productModels.add(
				new ProductModel("1","Regaliz","Regaliz negro enrollado",0.10,"https://www.lacasadelasgolosinas.com/3801-large_default/discos-negros-regaliz-haribo-2kg.jpg",
						100)
		);

		productModels.add(
				new ProductModel("2","Calaveras","Calaveras de pica pica",0.50,"https://www.lacasadelasgolosinas.com/7669-large_default/rellenolas-vidal-calaveras-65u.jpg",
						200)
		);

		productModels.add(
				new ProductModel("3","Monstruos","Gominolas de monstruos variados",1.50,"https://www.lacasadelasgolosinas.com/2906-large_default/caramelos-de-goma-fini-bolsa-surtido-terror-250u.jpg",
						50)
		);

		productModels.add(
				new ProductModel("4","Gusanos","Bolsa gominolas",1.50,"https://www.lacasadelasgolosinas.com/7365-large_default/caramelos-de-goma-jake-gusanos-pica-1kg.jpg",
						50)
		);

		productModels.add(
				new ProductModel("5","Pulpos","Caramelos de goma forma de pulpo",2.00,"https://www.lacasadelasgolosinas.com/2912-large_default/caramelos-de-goma-pulpos-pica-trolli-bolsa-1kg.jpg",
						100)
		);

		productModels.add(
				new ProductModel("6","Pingüinos","Gominolas forma de pingüino",2.50,"https://www.lacasadelasgolosinas.com/7356-large_default/caramelos-de-goma-jake-pingueinos-1kg.jpg",
						100)
		);

		productModels.add(
				new ProductModel("7","Anacondas","Anacondas de la muerte",1.50,"https://www.lacasadelasgolosinas.com/4747-large_default/caramelos-de-goma-trolli-anacondas-bolsa-1kg.jpg",
						100)
		);

		productModels.add(
				new ProductModel("8","Dentaduras","Gominolas con forma de dentadura",1.75,"https://www.lacasadelasgolosinas.com/4766-large_default/caramelos-de-goma-fini-bolsa-dentaduras-foam-250u.jpg",
						40)
		);

		productModels.add(
				new ProductModel("9","Chuche regaliz","Regaliz fino relleno",2.75,"https://www.lacasadelasgolosinas.com/3689-large_default/regaliz-fini-relleno-negro-tacos-1kg.jpg",
						50)
		);

		return productModels;
	}

	// funcion getUserData() con datos random de usuarios
	private static ArrayList<UserModel> getUserData() {
		ArrayList<UserModel> userModels = new ArrayList<>();
		// creamos los usuarios
		userModels.add(
				new UserModel("1","Jaume",26)
		);
		userModels.add(
				new UserModel("2","Fernando",19
				)
		);
		userModels.add(
				new UserModel("3","Ana Maria",30
				)
		);
		userModels.add(
				new UserModel("4","Luisa",58
				)
		);
		return userModels;
	}

	private static ArrayList<PurchaseModel> getTestPurchases() {

		ArrayList<PurchaseModel> purchaseModels = new ArrayList<>();
		return purchaseModels;
	}
}
