package com.bbva.hackaton.services;

import com.bbva.hackaton.models.UserModel;
import com.bbva.hackaton.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    // buscamos todos los usuario
    public List<UserModel> findAllUsers() {
        System.out.println("findUser en UserService");
        return this.userRepository.findAllUsers();
    }
}
