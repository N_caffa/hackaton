package com.bbva.hackaton.services;

import com.bbva.hackaton.models.PurchaseModel;
import com.bbva.hackaton.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;
    @Autowired
    ProductService productService;

    public List<PurchaseModel> findAll (){
        System.out.println("findAll de PurchaseService");

        return purchaseRepository.findAll();
    }

    public Optional<PurchaseModel> findById(String id){
        System.out.println("findById en PurchaseService");

        return this.purchaseRepository.findById(id);
    }


    public ServiceResponse addShop(PurchaseModel purchase) {
        System.out.println("addShop en ShopService");

        ServiceResponse result = new ServiceResponse();
        result.setPurchase(purchase);

//*validamos que no exista ya un carrito con esa ID
        System.out.println("Validamos que no exista una compra con el mismo ID");
        System.out.println("Id de la compra: " + purchase.getId());

        Optional<PurchaseModel> purchaseaux = this.findById(purchase.getId());

        if (purchaseaux.isPresent()){
            result.setMsg("Ya existe un carrito con esa ID");
            result.setResponseStatus(HttpStatus.BAD_REQUEST);
            return result;
        }

//Validamos que las cantidades que se compran son positiva
        for(Map.Entry<String, Integer> compraProductos : purchase.getPurchaseItems().entrySet()){
            if (compraProductos.getValue() < 1) {
                result.setMsg("La cantidad de producto no puede ser menor de 1");
                result.setResponseStatus(HttpStatus.BAD_REQUEST);
                return result;
            }
        }

//Calculamos el precio total del carrito y validamos que existan todos los productos
//Pendiente validar que haya stock suficiente  y hay que pensar como actualizar el stock
        float amount = 0;
        for(Map.Entry<String, Integer> compraProductos : purchase.getPurchaseItems().entrySet()){
            if (this.productService.findById(compraProductos.getKey()).isPresent() == false){
                System.out.println("el producto con la id: " + compraProductos.getKey() +
                        " no se encuentra en el sistema");
                result.setMsg("El producto " + compraProductos.getKey() + " no se encuentra en el sistema");
                result.setResponseStatus(HttpStatus.BAD_REQUEST);
                String ultimoProducto = compraProductos.getKey();
                for(Map.Entry<String, Integer> compraProductos2 : purchase.getPurchaseItems().entrySet()){
                    System.out.println(compraProductos2.getKey());
                    if(compraProductos2.getKey().equals(ultimoProducto)){
                        break;
                    }
                    System.out.println(compraProductos2.getValue());
                    this.productService.findById(compraProductos2.getKey()).get().sumaStock(compraProductos2.getValue());
                }
                return result;
            } else {
                amount +=
                        (this.productService.findById(compraProductos.getKey()).get().getPrice()
                                * compraProductos.getValue());
                Boolean pruebaStock = this.productService.findById(compraProductos.getKey()).get().restaStock(compraProductos.getValue());
                System.out.println(pruebaStock);
                if(pruebaStock==false){
                    System.out.println("Entra en el if con el pruebaStock");
                    String ultimoProducto = compraProductos.getKey();
                    System.out.println(ultimoProducto);
                    result.setMsg("La cantidad de producto supera el stock");
                    result.setResponseStatus(HttpStatus.BAD_REQUEST);
                    for(Map.Entry<String, Integer> compraProductos2 : purchase.getPurchaseItems().entrySet()){
                        System.out.println(compraProductos2.getKey());
                        if(compraProductos2.getKey().equals(ultimoProducto)){
                            System.out.println("Salgo del bucle");
                            result.setMsg("La cantidad de producto supera el stock");
                            result.setResponseStatus(HttpStatus.BAD_REQUEST);
                            return result;
                        }
                        System.out.println("Restauro stock");
                        this.productService.findById(compraProductos2.getKey()).get().sumaStock(compraProductos2.getValue());
                    }
                    return result;
                }
            }

        }


        purchase.setAmount(amount);
        this.purchaseRepository.save(purchase);

        result.setMsg("Compra añadida correctamente");
        result.setResponseStatus(HttpStatus.OK);
        return result;
    }

}
