package com.bbva.hackaton.services;

import com.bbva.hackaton.models.ProductModel;
import com.bbva.hackaton.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll() {
        System.out.println("findAll en ProductService");

        return this.productRepository.findAll();
    }

    public Optional<ProductModel> findById(String id){
        System.out.println("findById en ProductService ");

        return this.productRepository.findById(id);
    }
}
