package com.bbva.hackaton.services;

import com.bbva.hackaton.models.PurchaseModel;
import org.springframework.http.HttpStatus;

public class ServiceResponse {
    private String msg;
    private PurchaseModel purchase;
    private HttpStatus responseStatus;

    public ServiceResponse() {
    }

    public ServiceResponse(String msg, PurchaseModel purchase, HttpStatus responseStatus) {
        this.msg = msg;
        this.purchase = purchase;
        this.responseStatus = responseStatus;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public PurchaseModel getPurchase() {
        return purchase;
    }

    public void setPurchase(PurchaseModel purchase) {
        this.purchase = purchase;
    }

    public HttpStatus getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(HttpStatus responseStatus) {
        this.responseStatus = responseStatus;
    }
}
